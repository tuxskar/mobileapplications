package mediaPlayer.com;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import android.net.Uri;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MediaPlayerActivity extends Activity {
    private MediaPlayer mp;
    private TextView songname, acction, progressText;
    private SeekBar seekBar;
    private Intent intent;
    private UpdateSeekBarTask updateSeekBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        songname = (TextView) this.findViewById(R.id.songname);
        acction = (TextView) this.findViewById(R.id.acction);
        progressText = (TextView) this.findViewById(R.id.progress);

        mp = new MediaPlayer();
        
        seekBar = (SeekBar) this.findViewById(R.id.seekBar);
        intent = new Intent("org.openintents.action.PICK_FILE");
        
        seekBar = (SeekBar) this.findViewById(R.id.seekBar);
        intent = new Intent("org.openintents.action.PICK_FILE");

        acction.setText("Ready");
    }

    private void initMediaPlayerWithFileName(String fileName) {
        mp = new MediaPlayer();
        try {
            mp.setDataSource(fileName);
            mp.prepare();
            initSeekBar();
        } catch (IllegalArgumentException e) {
            acction.setText("Wrong file provided");
        } catch (IllegalStateException e) {
            acction.setText("Not ready for playing the file");
        } catch (IOException e) {
            acction.setText("Error while reading the file");
        }
    }

    private void initMediaPlayerWithUri(Uri uri) {
        // remove the actual mediaPlayer to create the new one
    	if (mp.isPlaying()){
    		mp.stop();
    	}
    	mp = new MediaPlayer();
        mp = MediaPlayer.create(this, uri);
        initSeekBar();
    }

    private void initSeekBar() {
        // create the SeekBar
        int mediaDuration = mp.getDuration();
        seekBar.setMax(mediaDuration);
        seekBar.setVisibility(View.VISIBLE);

        // start the AsyncTask that updates the SeekBar
        updateSeekBar = new UpdateSeekBarTask();
        updateSeekBar.execute(mp);

        // set the change listener for the SeekBar
        OnSeekBarChangeListener seekBarChangeListener = new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                if (fromUser) {
                    updateSeekBar.cancel(true);
                    mp.seekTo(progress);
                    setSongProgress(progress);
                    updateSeekBar = new UpdateSeekBarTask();
                    updateSeekBar.execute(mp);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
    }

    public void start(View v) {
        mp.start();
        acction.setText("Playing song");
    }

    public void stop(View v) throws IllegalStateException, IOException {
        mp.pause();
        mp.seekTo(0);
        setSongProgress(0);
        acction.setText("Song stopped");
    }

    public void pause(View v) {
        if (mp.isPlaying()) {
            mp.pause();
            acction.setText("Song paused");
        }
    }

    public void pickFile(View v) {
        startActivityForResult(intent, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            // obtain the filename
            String fileName = data.getDataString();
            initMediaPlayerWithFileName(fileName);
            songname.setText(fileName);
        }
    }

    public void fileFromWeb(View v) {
        acction.setText("Picking file from the web, so cool!");
        Uri uri = Uri
                .parse("http://www.emp3world.com/to_download.php?id=48687");
        initMediaPlayerWithUri(uri);
        songname.setText("It's my life from web");
    }

    public void setSongProgress(int progressInMs) {
        // update seekbar
        seekBar.setProgress(progressInMs);

        // update progress text
        String humanReadableProgress = String.format(
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(progressInMs),
                TimeUnit.MILLISECONDS.toSeconds(progressInMs)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                                .toMinutes(progressInMs)));
        progressText.setText(humanReadableProgress);
    }

    private class UpdateSeekBarTask extends
            AsyncTask<MediaPlayer, Integer, Void> {

        @Override
        protected Void doInBackground(MediaPlayer... params) {
            MediaPlayer mp = params[0];
            while (!isCancelled()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                if (mp.isPlaying()) {
                    int currentSeek = mp.getCurrentPosition();
                    publishProgress(currentSeek);
                }
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            setSongProgress(progress[0]);
        }
    }
}
