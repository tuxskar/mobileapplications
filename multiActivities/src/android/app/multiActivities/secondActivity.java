package android.app.multiActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class secondActivity extends Activity {
    /** Called when the activity is first created. */
    Button toMain;
    String from;
    TextView t;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondlayout);
        toMain = (Button) this.findViewById(R.id.toMain);
        from = null;
        t = (TextView) this.findViewById(R.id.secondTextView);
        Intent intent = this.getIntent();
        t.setText(intent.getStringExtra("from"));
    }
   public void toMain(View v){
	   from = "I come second";
       Intent intent = new Intent(secondActivity.this, MultiActivitiesActivity.class);
       intent.putExtra("from",this.from);
       startActivity(intent);
		this.finishActivity(RESULT_OK);
   }    
}
