package android.app.multiActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MultiActivitiesActivity extends Activity {
    /** Called when the activity is first created. */
    Button toSecond;
    String from;
    TextView t;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        toSecond = (Button) this.findViewById(R.id.toSecond);
        from = null;
        t = (TextView) this.findViewById(R.id.mainTextView1);
        
   }

   public void toSecond(View v){
	   from = "I come main";
       Intent intent = new Intent(MultiActivitiesActivity.this, SwiperActivity.class);
       intent.putExtra("from", from);
       this.startActivityForResult(intent, 0);
   }   
   
   @Override
   protected void onActivityResult(int requestCode, int resultCode,
           Intent data) {
       String extra = data.getStringExtra("from");
       t.setText(extra);
   }
   
}

