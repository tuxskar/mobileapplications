package android.app.swiperReader;

import java.util.LinkedList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SRMainActivity extends Activity {
	private static final int SRACTIVITY = 2;
	/** Called when the activity is first created. */
	TextView fileName;
	String text;
	LinkedList<String> textList;
	boolean textReady = false;
	int index = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		fileName = (TextView) this.findViewById(R.id.textView1);
		text = new String();
		textList = new LinkedList<String>();
		textList.add("1º We are more often treacherous through weakness than through calculation.  ~Francois De La Rochefoucauld");
		textList.add("2º A man with one watch knows what time it is; a man with two watches is never quite sure.  ~Lee Segall");
		textList.add("3º Begin at the beginning and go on till you come to the end; then stop.  ~Lewis Carrol, Alice in Wonderland");
		textList.add("4º Believe those who are seeking the truth.  Doubt those who find it.  ~Andre Gide");
		textList.add("5º Beware lest you lose the substance by grasping at the shadow.  ~Aesop");
		textList.add("6º Only that in you which is me can hear what I'm saying.  ~Baba Ram Dass");
		textList.add("7º I am a part of all that I have met.  ~Alfred Lord Tennyson");
		textList.add("8º There's more to the truth than just the facts.  ~Author Unknown");
		textList.add("9º Even a clock that does not work is right twice a day.  ~Polish Proverb ");
		textList.add("10º Losing an illusion makes you wiser than finding a truth.  ~Ludwig Börne ");
		textList.add("11º If a man who cannot count finds a four-leaf clover, is he lucky?  ~Stanislaw J. Lec ");
		textList.add("12º We are all but recent leaves on the same old tree of life and if this life has adapted itself to new functions and conditions, it uses the same old basic principles over and over again.  There is no real difference between the grass and the man who mows it.  ~Albert Szent-Györgyi ");
		textList.add("13º Sometimes it's necessary to go a long distance out of the way in order to come back a short distance correctly.  ~Edward Albee ");
		textList.add("14º When the student is ready, the master appears.  ~Buddhist Proverb ");
		textList.add("15º A gun gives you the body, not the bird.  ~Henry David Thoreau ");
		textList.add("16º Before enlightenment - chop wood, carry water.  After enlightenment - chop wood, carry water.  ~Zen Buddhist Proverb ");
		textList.add("17º Many men go fishing all of their lives without knowing that it is not fish they are after.  ~Henry David Thoreau ");
		textList.add("18º Wars and elections are both too big and too small to matter in the long run.  The daily work - that goes on, it adds up.  ~Barbara Kingsolver, Animal Dreams ");
		textList.add("19º I tell you everything that is really nothing, and nothing of what is everything, do not be fooled by what I am saying.  Please listen carefully and try to hear what I am not saying.  ~Charles C. Finn ");
		textList.add("20º Oh, Heaven, it is mysterious, it is awful to consider that we not only carry a future Ghost within us; but are, in very deed, Ghosts!  ~Thomas Carlyle ");
		textList.add("21º Knock on the sky and listen to the sound.  ~Zen Saying ");
		textList.add("22º The fish trap exists because of the fish.  Once you've gotten the fish you can forget the trap.  The rabbit snare exists because of the rabbit.  Once you've gotten the rabbit, you can forget the snare.  Words exist because of meaning.  Once you've gotten the meaning, you can forget the words.  Where can I find a man who has forgotten words so I can talk with him?  ~Chuang Tzu ");
		textList.add("23º By daily dying I have come to be.  ~Theodore Roethke ");
		textList.add("24º There are some remedies worse than the disease.  ~Publilius Syrus ");
		textList.add("25º You never know what is enough, until you know what is more than enough.  ~William Blake, Proverbs of Hell ");
		textList.add("26º It requires a great deal of faith for a man to be cured by his own placebos.  ~John L. McClenahan ");
		textList.add("27º What you see, yet can not see over, is as good as infinite.  ~Thomas Carlyle, Sartor Resartus, Book II, chapter 1 ");
		textList.add("28º Philosophy is life's dry-nurse, who can take care of us - but not suckle us.  ~Soren Kierkegaard ");
		textList.add("29º Men are probably nearer the central truth in their superstitions than in their science.  ~Henry David Thoreau");
		fileName.setText("Dummy data ready");
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case SRACTIVITY:
			if (resultCode == RESULT_OK && data != null) {
				String returned = data.getStringExtra("to");
				if (returned.equals("next")) {
					this.startReadActivity(nextLine());
				} else if (returned.equals("previous")) {
					// previousLine
					this.startReadActivity(previousLine());
				} else {
					// actialLine
					this.startReadActivity(actualLine());
				}
			}
			break;
		}
	}

	public String nextLine() {
		if (index < textList.size() - 1) {
			index++;
		}
		return textList.get(index);

	}

	public String previousLine() {
		if (index > 0) {
			index--;
		}
		return textList.get(index);
	}

	public String actualLine() {
		return textList.get(index);
	}

	public void startRead(View v) {
		Intent intent = new Intent(SRMainActivity.this, SRActivity.class);
		intent.putExtra("line", actualLine());
		this.startActivityForResult(intent, SRMainActivity.SRACTIVITY);
	}

	public void startReadActivity(String str) {
		Intent intent = new Intent(SRMainActivity.this, SRActivity.class);
		intent.putExtra("line", str);
		this.startActivityForResult(intent, SRMainActivity.SRACTIVITY);
	}

}
