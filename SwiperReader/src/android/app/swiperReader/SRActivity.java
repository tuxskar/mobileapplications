package android.app.swiperReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class SRActivity extends Activity implements OnClickListener, OnGestureListener{
	private static final int SWIPE_MIN_DISTANCE = 20;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private String to = "actual";
	private GestureDetector gesturedetector = null;

	private TextView text;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reading);
		text = (TextView) this.findViewById(R.id.lines);
		Intent intent = this.getIntent();
		text.setText(intent.getStringExtra("line"));

	    gesturedetector = new GestureDetector(this, this);
	}
	
	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				Toast.makeText(SRActivity.this, "Left Swipe",
						Toast.LENGTH_SHORT).show();
				
				nextLine(text);
			} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				Toast.makeText(SRActivity.this, "Right Swipe",
						Toast.LENGTH_SHORT).show();
				previousLine(text);
			}
		} catch (Exception e) {
			// nothing
		}
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
            return gesturedetector.onTouchEvent(event);
    }


	public void nextLine(View v){
		Intent intent = new Intent();
		to = "next";
		intent.putExtra("to", to);
		setResult(RESULT_OK, intent);
		finish();
	}
	
	public void previousLine(View v){
		Intent intent = new Intent();
		to = "previous";
		intent.putExtra("to", to);
		setResult(RESULT_OK, intent);
		finish();
	}
}
